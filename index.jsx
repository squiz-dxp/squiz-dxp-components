const Dashboard = require('./components/Dashboard');
const Grid      = require('./components/Grid');

module.exports = {
  Dashboard,
  Grid
};