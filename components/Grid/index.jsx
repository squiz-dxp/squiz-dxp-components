const React = require('react');

module.exports = ({columns, Trans}) => {
  const arr   = Array.from({length: columns}, (v, i) => i + 1);
  const elems = arr.map(num => {
    return (
      <div className="col" key={num}>
        <Trans i18nKey="component.grid.column">Column #{{num}}</Trans>
      </div>
    )
  });
  return (
    <div className="row">
      {elems}
    </div>
  );
};