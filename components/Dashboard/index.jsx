const React = require('react');

module.exports = ({section1, section2, Trans}) => {
  return (
    <div className="container">
      <h2><Trans>Dashboard</Trans></h2>
      <div className="section1 row">{section1}</div>
      <div className="section2 row">{section2}</div>
    </div>
  );
}